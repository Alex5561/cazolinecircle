// Fill out your copyright notice in the Description page of Project Settings.


#include "WeaponBase.h"
#include "Engine/World.h"
#include "Kismet/GameplayStatics.h"
#include "DrawDebugHelpers.h"


// Sets default values
AWeaponBase::AWeaponBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Scene"));
	RootComponent = SceneComponent;

	WeaponMesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("SkeletalMesh"));
	WeaponMesh->SetGenerateOverlapEvents(false);
	WeaponMesh->SetCollisionProfileName(TEXT("NoCollision"));
	WeaponMesh->SetupAttachment(RootComponent);

}

// Called when the game starts or when spawned
void AWeaponBase::BeginPlay()
{
	Super::BeginPlay();

	check(WeaponMesh);
	MaxCurrentAmmo = CurrentAmmo;
	
}

// Called every frame
void AWeaponBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AWeaponBase::FireWeapon()
{
	if (CurrentAmmo > 0 && !Reloading)
	{
		Fire = true;
		MakeShot();
		GetWorldTimerManager().SetTimer(ShotTimerHandle, this, &AWeaponBase::MakeShot, FireInterval, true);
	}
	
}

void AWeaponBase::StopFire()
{
	Fire = false;
	GetWorldTimerManager().ClearTimer(ShotTimerHandle);
}

void AWeaponBase::ReloadingWeapon()
{
	Reloading = true;
	GetWorldTimerManager().SetTimer(ReloadTimerHandle, this, &AWeaponBase::StopReloadWeapon, ReloadInterval, false);
	if (AnimReload)
	{
		WeaponMesh->PlayAnimation(AnimReload, false);
	}
	
}

void AWeaponBase::StopReloadWeapon()
{
	Reloading = false;
	GetWorldTimerManager().ClearTimer(ReloadTimerHandle);
}

void AWeaponBase::MakeShot()
{
	if (!GetWorld()) return;
	 FTransform SocketTransform = WeaponMesh->GetSocketTransform(SocketGun);
	 FVector TraceStart = SocketTransform.GetLocation();
	 FVector TraceEnd = ShootEndLocation + FVector(0, 0, 135.f) + Dispersion;

	DrawDebugLine(GetWorld(), TraceStart, TraceEnd, FColor::Red, false, 3.0f, 0, 3.0f);
	FHitResult HitResult;
	FCollisionQueryParams Ignore;
	Ignore.AddIgnoredActor(this);
	GetWorld()->LineTraceSingleByChannel(HitResult, TraceStart, TraceEnd, ECC_Visibility,Ignore);
	if (AnimFire)
	{
		WeaponMesh->PlayAnimation(AnimFire, false);
	}
	
	if (HitResult.bBlockingHit)
	{
		UGameplayStatics::ApplyPointDamage(HitResult.GetActor(), Damage, HitResult.TraceStart, HitResult, GetInstigatorController(), this, NULL);
		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), ImpactEffect, FTransform(HitResult.ImpactNormal.Rotation(),HitResult.ImpactPoint, FVector(1.0f)));
	}
	CurrentAmmo -= 1;
	if (CurrentAmmo <= 0)
	{
		StopFire();
	}
}



