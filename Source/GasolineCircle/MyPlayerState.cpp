// Fill out your copyright notice in the Description page of Project Settings.


#include "MyPlayerState.h"
#include "Kismet/GameplayStatics.h"
#include "MyCharacter.h"
#include "WeaponBase.h"


void AMyPlayerState::BeginPlay()
{
	Super::BeginPlay();

	AMyCharacter* myChar = Cast<AMyCharacter>(UGameplayStatics::GetPlayerCharacter(GetWorld(),0));
	if (myChar)
	{
		MyCharacterREF = myChar;
	}
	SetAmmo(31);
}

AWeaponBase* AMyPlayerState::GetHoldGun()
{
	return HoldGun;
}

AWeaponBase* AMyPlayerState::GetWeapon1()
{
	return Weapon1;
}


AWeaponBase* AMyPlayerState::GetWeapon2()
{
	return Weapon2;
}

int32 AMyPlayerState::GetAmmo()
{
	return AmountAmmo;
}



 void AMyPlayerState::SetHoldGun(AWeaponBase* Weapon)
{
	 HoldGun = Weapon;
	 OnSetWeapon.Broadcast();
}

 void AMyPlayerState::SetWeapon1(AWeaponBase* Weapon)
 {
	 Weapon1 = Weapon;
	 OnSetWeapon.Broadcast();
 }

 void AMyPlayerState::SetWeapon2(AWeaponBase* Weapon)
 {
	 Weapon2 = Weapon;
	 OnSetWeapon.Broadcast();
 }

 void AMyPlayerState::SetAmmo(int32 Ammo)
 {
	 AmountAmmo += Ammo;
	 OnChangeAmmo.Broadcast();
 }









