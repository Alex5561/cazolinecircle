// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "HealthComponent.h"
#include "GameFramework/Character.h"
#include "WeaponBase.h"
#include "MyCharacter.generated.h"

class AMyPlayerState;

UCLASS()
class GASOLINECIRCLE_API AMyCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AMyCharacter();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	FORCEINLINE class UCameraComponent* GetTopDownCameraComponent() const { return TopDownCameraComponent; }
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }

	

private:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		class UCameraComponent* TopDownCameraComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		class USpringArmComponent* CameraBoom;

	FTimerHandle MontageFireInterval;//Монтаж Выстрела Таймер
	FTimerHandle MontageEquipWeapon;//Монтаж Смены Оружия Таймер
	FTimerHandle ReloadTimer;//Монтаж Перезарядки Таймер
	FTimerHandle DispersionTimer;//Таймер Разброс Оружия
public:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Health", meta = (AllowPrivateSuccess = "true"))
		UHealthComponent* Health;//Компонент Здоровья

	FHitResult TraceHitResult;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon")
	AMyPlayerState* PlayerStateREF;//PlayerState переменная

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon")
	float IntervalMontageFire;  

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon")
		bool Fire = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon")
		bool Reload = false;
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Weapon")
		bool Aiming = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "MovementState")
		bool Sprint = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Live")
		bool Dead = false;

	int32 Money = 0;//Монеты для покупки патронов

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Cursor")
		UMaterialInterface* CursorMaterial = nullptr;//Курсоа
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Cursor")
		FVector CursorSize = FVector(20.0f, 40.0f, 40.0f);// Курсор
		
	UDecalComponent* Cursor = nullptr;// Курсор

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WeaponRef")
		TSubclassOf<AWeaponBase>RifleWeapon;//Класс спавна первого оружия
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WeaponRef")
		TSubclassOf<AWeaponBase>SniperWeapon;//Класс спавна второго оружия

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AnimMontage")
		UAnimMontage* FireHip = nullptr;//Монтаж Стрельбы от бедра
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AnimMontage")
		UAnimMontage* FireIrosight = nullptr;//Монтаж стрельбы при прицеливание
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AnimMontage")
		UAnimMontage* ReloadMontage = nullptr;//Монтаж Перезарядки
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AnimMontage")
		UAnimMontage* EquipWeapon = nullptr;// Монтаж смены оружия

	
	void SetPlayerStateREF();//cast Player State
	void MovementCursor();//movement of the cursor with the mouse
	void SetCharacterSpeed();//Update Speed Character
	void RotationCharacter();// Rotate the character towards the cursor
	UFUNCTION()
	void CharDead();//Dead Function
	UFUNCTION()
	void UpdateWeaponDisplay();//Weapon update when swapping
	void InitWeapon();//Weapon initialization, launch at the start of the game
	void FireWeapon(bool Firing);//Shooting with the current weapon
	void ReloadingWeapon();//Reload with the current weapon
	void PlayMontageFiring();//Playback of the Shooting Montage, depending on the shooting state (from the hip, aim)
	void SetCurrentAmountAmmoWeapon(); //Calculation of the required number of cartridges to reload the current weapon
	UFUNCTION()
	void WeaponChange1();//Input
	UFUNCTION()
	void WeaponChange2();//Input
	UFUNCTION()
	void SetChangeWeapon1();//Input
	UFUNCTION()
	void SetChangeWeapon2();//Input
	UFUNCTION(BlueprintCallable, BlueprintPure)
	int32 MoneyAmount();//Return Money
	UFUNCTION(BlueprintCallable)
	void SetMoneyAmount(int32 MoneyDicr);//PickUP Money, Calculate
	void StopReload();//ClearTimer Reloading
	void Dispersion();



	virtual float TakeDamage(float DamageAmount, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, AActor* DamageCauser) override;

};
