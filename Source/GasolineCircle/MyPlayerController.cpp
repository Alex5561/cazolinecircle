// Fill out your copyright notice in the Description page of Project Settings.


#include "MyPlayerController.h"
#include "MyCharacter.h"
#include "Kismet/GameplayStatics.h"
#include "MyPlayerState.h"
#include "GameFramework/PlayerController.h"





void AMyPlayerController::BeginPlay()
{
	Super::BeginPlay();

	SetREFCharacter();
	SetREFPlayerState();
}

void AMyPlayerController::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	GetCharacterREF()->RotationCharacter();
	if (GetMyPlayerStateREF()->GetHoldGun())
	{
		GetMyPlayerStateREF()->GetHoldGun()->ShootEndLocation = GetCharacterREF()->TraceHitResult.Location;
	}	
}

void AMyPlayerController::SetupInputComponent()
{
	Super::SetupInputComponent();

	check(InputComponent);

	InputComponent->BindAxis("Forward", this, &AMyPlayerController::MovementCharacterForward);
	InputComponent->BindAxis("Right", this, &AMyPlayerController::MovementCharacterRight);

	InputComponent->BindAction("Aiming", IE_Pressed, this, &AMyPlayerController::Aiming);
	InputComponent->BindAction("Aiming", IE_Released, this, &AMyPlayerController::UnAiming);

	InputComponent->BindAction("Sprint", IE_Pressed, this, &AMyPlayerController::Sprint);
	InputComponent->BindAction("Sprint", IE_Released, this, &AMyPlayerController::UnSprint);

	InputComponent->BindAction("Fire", IE_Pressed, this, &AMyPlayerController::FireInput);
	InputComponent->BindAction("Fire", IE_Released, this, &AMyPlayerController::UnFireInput);

	InputComponent->BindAction("Reload", IE_Pressed, this, &AMyPlayerController::Reload);

	InputComponent->BindAction("Weapon1", IE_Pressed, this, &AMyPlayerController::SetWeapon1);
	InputComponent->BindAction("Weapon2", IE_Pressed, this, &AMyPlayerController::SetWeapon2);

}



void AMyPlayerController::SetREFCharacter()
{
	AMyCharacter* myChar = Cast<AMyCharacter>(UGameplayStatics::GetPlayerCharacter(GetWorld(), 0));
	if (myChar)
	{
		MyCharacterREF = myChar;
	}
}

void AMyPlayerController::SetREFPlayerState()
{
	AMyPlayerState* myPlayerState = Cast<AMyPlayerState>(PlayerState);
	if (myPlayerState)
	{
		MyPlayerStateREF = myPlayerState;
	}
}

AMyCharacter* AMyPlayerController::GetCharacterREF()
{
	return MyCharacterREF;
}

AMyPlayerState* AMyPlayerController::GetMyPlayerStateREF()
{
	return MyPlayerStateREF;
}

void AMyPlayerController::MovementCharacterForward(float input)
{
	if (!GetCharacterREF()->Dead)
	{
		GetCharacterREF()->AddMovementInput(GetCharacterREF()->GetActorForwardVector(), input);
		AxisX = input;
	}
}

void AMyPlayerController::MovementCharacterRight(float input)
{
	if (!GetCharacterREF()->Dead) 
	{
			GetCharacterREF()->AddMovementInput(GetCharacterREF()->GetActorRightVector(), input);
			AxisY = input;
	}
	
}

void AMyPlayerController::Aiming()
{
	if (!GetCharacterREF()->Dead)
	{
		if (GetCharacterREF()->Sprint)
		{
			GetCharacterREF()->Sprint = false;
			GetCharacterREF()->SetCharacterSpeed();
		}
		else
			GetCharacterREF()->Aiming = true;
		GetCharacterREF()->SetCharacterSpeed();
	}
}

void AMyPlayerController::UnAiming()
{
	if (!GetCharacterREF()->Dead)
	{
		GetCharacterREF()->Aiming = false;
		GetCharacterREF()->SetCharacterSpeed();
	}
}

void AMyPlayerController::Sprint()
{
	if (!GetCharacterREF()->Dead && !GetCharacterREF()->Fire &&!GetCharacterREF()->Reload)
	{
		if (AxisX==1 && AxisY == 0)
		{
			if (GetCharacterREF()->Aiming)
			{
				GetCharacterREF()->Aiming = false;
				GetCharacterREF()->Sprint = true;
			}
			else
				GetCharacterREF()->Sprint = true;
			GetCharacterREF()->SetCharacterSpeed();
		}
	}
}

void AMyPlayerController::UnSprint()
{
	if (!GetCharacterREF()->Dead)
	{
		GetCharacterREF()->Sprint = false;
		GetCharacterREF()->SetCharacterSpeed();
	}
}

void AMyPlayerController::FireInput()
{
	GetCharacterREF()->FireWeapon(true);
}

void AMyPlayerController::UnFireInput()
{
	GetCharacterREF()->FireWeapon(false);
}

void AMyPlayerController::Reload()
{
	GetCharacterREF()->ReloadingWeapon();
}

void AMyPlayerController::SetWeapon1()
{
	GetCharacterREF()->WeaponChange1();
}

void AMyPlayerController::SetWeapon2()
{
	GetCharacterREF()->WeaponChange2();
}







