// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Particles/ParticleSystem.h"
#include "WeaponBase.generated.h"



UCLASS()
class GASOLINECIRCLE_API AWeaponBase : public AActor
{
	GENERATED_BODY()

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	class USceneComponent* SceneComponent = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	class USkeletalMeshComponent* WeaponMesh = nullptr;
public:	
	// Sets default values for this actor's properties
	AWeaponBase();

	

private:
	FTimerHandle ShotTimerHandle;
	FTimerHandle ReloadTimerHandle;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WeaponSettings")
		float FireInterval = 0.f;//BP
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WeaponSettings")
		float ReloadInterval = 0.f;//BP
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WeaponSettings")
		int32 CurrentAmmo = 0.f;//BP
	UPROPERTY(VisibleAnywhere, Category = "WeaponSettings")
		int32 MaxCurrentAmmo;//BeginPlay
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WeaponSettings")
		float Damage = 0.0f;//BP
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WeaponSettings")
		UTexture2D* WeaponTexture = nullptr;//BP Widget
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FName SocketGun = "Muzzle";//Fire Start

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WeaponAnim")
		UAnimSequence* AnimFire = nullptr;//Anim
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WeaponAnim")
		UAnimSequence* AnimReload = nullptr;//Anim
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WeaponSettings")
		UParticleSystem* ImpactEffect = nullptr;//Emmiter Hit

	

	FVector ShootEndLocation;
	FVector Dispersion;
	


	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WeaponState")
		bool Fire = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WeaponState")
		bool Reloading = false;

	
	UFUNCTION(BlueprintCallable)
	void FireWeapon();//Start Fire
	void StopFire();
	void ReloadingWeapon();
	void StopReloadWeapon();
	void MakeShot();//Shot Start
	


};
