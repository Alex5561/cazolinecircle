// Fill out your copyright notice in the Description page of Project Settings.


#include "PickUPBase.h"
#include "Components/PrimitiveComponent.h"
#include "CollisionQueryParams.h"
#include "MyCharacter.h"
#include "Math/UnrealMathUtility.h"
#include "Kismet/GameplayStatics.h"
#include "DrawDebugHelpers.h"

// Sets default values
APickUPBase::APickUPBase()
{
 	
	PrimaryActorTick.bCanEverTick = true;

	DefaultScene = CreateDefaultSubobject<USceneComponent>(TEXT("DefaultSceneCom"));
	RootComponent = DefaultScene;

	BoxCollision = CreateDefaultSubobject<UBoxComponent>(TEXT("BoxCollisionCom"));
	BoxCollision->SetBoxExtent(FVector(50.0, 50.0, 25.0));
	BoxCollision->SetGenerateOverlapEvents(true);
	BoxCollision->SetCollisionProfileName("BlockALL");
	BoxCollision->SetCollisionResponseToChannel(ECollisionChannel::ECC_Pawn, ECollisionResponse::ECR_Overlap);
	BoxCollision->SetupAttachment(RootComponent);

	StaticMeshPickUP = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMesh"));
	StaticMeshPickUP->SetCollisionProfileName("NoCollision");
	StaticMeshPickUP->SetupAttachment(RootComponent);


}


void APickUPBase::BeginPlay()
{
	Super::BeginPlay();

	SetLocationBegin();

	BoxCollision->OnComponentBeginOverlap.AddDynamic(this, &APickUPBase::BulletCollisionBoxBeginOverlap);
	BoxCollision->OnComponentEndOverlap.AddDynamic(this, &APickUPBase::BulletCollisionBoxEndOverlap);
	
	AMyCharacter* myChar = Cast<AMyCharacter>(UGameplayStatics::GetPlayerCharacter(GetWorld(),0));
	if (myChar)
	{
		CharREF = myChar;
	}
	
}


void APickUPBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void APickUPBase::SetLocationBegin()
{
	DrawDebugLine(GetWorld(), GetActorLocation(), GetActorLocation() + FVector(0.f, 0.f, -1000.f), FColor::Red, false, 3.0f, 0, 3.0f);
	
	FHitResult HitResult;
	
	
	FCollisionQueryParams Ignore;
	Ignore.AddIgnoredActor(this);
	GetWorld()->LineTraceSingleByChannel(HitResult, GetActorLocation(), GetActorLocation() + FVector(0.0,0.0,-1500.0), ECC_GameTraceChannel3, Ignore);
	if (HitResult.bBlockingHit)
	{
		SetActorLocation(HitResult.Location, false, 0, ETeleportType::None);
	}
}

void APickUPBase::SetDepth(bool SetRender)
{
	this->StaticMeshPickUP->SetRenderCustomDepth(SetRender);
}

void APickUPBase::SelectionOnItems()
{

}

void APickUPBase::BulletCollisionBoxBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	SetDepth(true);
	if (OtherActor == CharREF)
	{
		int32 RandomMoney = FMath::RandRange(5, 10);
		CharREF->Money = RandomMoney;
		this->Destroy();
	}
}

void APickUPBase::BulletCollisionBoxEndOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	SetDepth(false);
}

