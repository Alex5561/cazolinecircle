// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerState.h"
#include "MyPlayerState.generated.h"


DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnChangeAmmo);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnSetWeapon);

class AWeaponBase;
class AMyCharacter;

UCLASS()
class GASOLINECIRCLE_API AMyPlayerState : public APlayerState
{
	GENERATED_BODY()

private:

	virtual void BeginPlay() override;
	
	AWeaponBase* HoldGun;//with Gun
	AWeaponBase* Weapon1;
	AWeaponBase* Weapon2;
	int32 AmountAmmo;//BackPuck Ammo
	

public:
	
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CharRef")
	AMyCharacter* MyCharacterREF;//Variable

	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "WeaponDisplay")
		FOnChangeAmmo OnChangeAmmo;
	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "WeaponDisplay")
		FOnSetWeapon OnSetWeapon;

	

	UFUNCTION(BlueprintCallable,BlueprintPure)
	AWeaponBase* GetHoldGun();//Return
	UFUNCTION(BlueprintCallable, BlueprintPure)
	AWeaponBase* GetWeapon1();//Return
	UFUNCTION(BlueprintCallable, BlueprintPure)
	AWeaponBase* GetWeapon2();//Return
	UFUNCTION(BlueprintCallable)
	int32 GetAmmo();//Return
	

	UFUNCTION()
	void SetHoldGun(AWeaponBase* Weapon);//Setter
	UFUNCTION()
	void SetWeapon1(AWeaponBase* Weapon);//Setter
	UFUNCTION()
	void SetWeapon2(AWeaponBase* Weapon);//Setter
	UFUNCTION(BlueprintCallable)
	void SetAmmo(int32 Ammo);//Setter


	

	
	
};
