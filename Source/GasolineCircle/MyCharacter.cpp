// Fill out your copyright notice in the Description page of Project Settings.


#include "MyCharacter.h"
#include "UObject/ConstructorHelpers.h"
#include "Camera/CameraComponent.h"
#include "Components/DecalComponent.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/PlayerController.h"
#include "GameFramework/SpringArmComponent.h"
#include "Materials/Material.h"
#include "Kismet/GameplayStatics.h"
#include "WeaponBase.h"
#include "MyPlayerController.h"
#include "MyPlayerState.h"
#include "Animation/AnimInstance.h"
#include "Kismet/KismetMathLibrary.h"

// Sets default values
AMyCharacter::AMyCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;

	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	bUseControllerRotationPitch = false;
	bUseControllerRotationRoll = false;
	bUseControllerRotationYaw = false;

	GetCharacterMovement()->bOrientRotationToMovement = false; 
	GetCharacterMovement()->RotationRate = FRotator(0.f, 640.f, 0.f);
	GetCharacterMovement()->bConstrainToPlane = true;
	GetCharacterMovement()->bSnapToPlaneAtStart = true;

	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->SetUsingAbsoluteRotation(true);
	CameraBoom->TargetArmLength = 800.f;
	CameraBoom->SetRelativeRotation(FRotator(-60.f, 0.f, 0.f));
	CameraBoom->bDoCollisionTest = false;

	TopDownCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("TopDownCamera"));
	TopDownCameraComponent->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	TopDownCameraComponent->bUsePawnControlRotation = false;

	Health = CreateDefaultSubobject<UHealthComponent>(TEXT("HealthComponent"));
	if (Health)
	{
		Health->OnDead.AddDynamic(this, &AMyCharacter::CharDead);
	}
	
	
}

// Called when the game starts or when spawned
void AMyCharacter::BeginPlay()
{
	Super::BeginPlay();

	if (CursorMaterial)
	{
		Cursor = UGameplayStatics::SpawnDecalAtLocation(GetWorld(), CursorMaterial, CursorSize, FVector(0));
	}
	SetPlayerStateREF();
	PlayerStateREF->OnSetWeapon.AddDynamic(this, &AMyCharacter::UpdateWeaponDisplay);
	InitWeapon();
}

// Called every frame
void AMyCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	
	MovementCursor();
}


void AMyCharacter::SetPlayerStateREF()
{
	AMyPlayerState* myPS = Cast<AMyPlayerState>(GetPlayerState());
	if (myPS)
	{
		PlayerStateREF = myPS;
	}
}

void AMyCharacter::MovementCursor()
{
	if (!Dead)
	{
		if (Cursor)
		{
			AMyPlayerController* myPC = Cast<AMyPlayerController>(GetController());
			if (myPC)
			{
				myPC->GetHitResultUnderCursor(ECC_GameTraceChannel1, true, TraceHitResult);
				FVector CursorFV = TraceHitResult.ImpactNormal;
				FRotator CursorR = CursorFV.Rotation();

				Cursor->SetWorldLocation(TraceHitResult.Location);
				Cursor->SetWorldRotation(CursorR);
			}

		}
	}
}

void AMyCharacter::SetCharacterSpeed()
{
	float CurrentSpeed = 600.f;
	if(Aiming && !Sprint)
	{
		CurrentSpeed = 300.f;
	}
	if(!Aiming && Sprint)
	{
		CurrentSpeed = 800.f;
	}
	GetCharacterMovement()->MaxWalkSpeed = CurrentSpeed;
}

void AMyCharacter::RotationCharacter()
{
	if (!Dead)
	{
		APlayerController* myController = UGameplayStatics::GetPlayerController(GetWorld(), 0);
		if (myController)
		{
			FHitResult ResultHit;
			myController->GetHitResultUnderCursor(ECC_GameTraceChannel1, true, ResultHit);
			float FindRotaterResultYaw = UKismetMathLibrary::FindLookAtRotation(GetActorLocation(), ResultHit.Location).Yaw;
			SetActorRotation(FQuat(FRotator(0.0f, FindRotaterResultYaw, 0.0f)));
			
		}
	}
}

void AMyCharacter::CharDead()
{
	Dead = true;
	PlayerStateREF->GetHoldGun()->StopFire();
	FireWeapon(false);
	if (Cursor)
	{
		Cursor->SetVisibility(false);
	}
	this->SetLifeSpan(15.0f);
	if (GetController())
	{
		GetController()->UnPossess();
	}
	
}

void AMyCharacter::UpdateWeaponDisplay()
{
	if (PlayerStateREF->GetHoldGun())
	{
			FAttachmentTransformRules HoldGunWeapon(EAttachmentRule::SnapToTarget, false);
			PlayerStateREF->GetHoldGun()->AttachToComponent(GetMesh(), HoldGunWeapon, FName("WeaponHoldGun"));
	}
	if (PlayerStateREF->GetWeapon1())
	{
			FAttachmentTransformRules Weapon1Gun(EAttachmentRule::SnapToTarget, false);
			PlayerStateREF->GetWeapon1()->AttachToComponent(GetMesh(), Weapon1Gun, FName("Weapon1"));
	}
	if (PlayerStateREF->GetWeapon2())
	{
			FAttachmentTransformRules Weapon2Gun(EAttachmentRule::SnapToTarget, false);
			PlayerStateREF->GetWeapon2()->AttachToComponent(GetMesh(), Weapon2Gun, FName("Weapon2"));
	}
	
	
}

void AMyCharacter::InitWeapon()
{
	if (RifleWeapon)
	{
		AWeaponBase* myHG = GetWorld()->SpawnActor<AWeaponBase>(RifleWeapon, FVector(0), FRotator(0));
		if (myHG)
		{
			PlayerStateREF->SetHoldGun(myHG);
		}
	}
	if (SniperWeapon)
	{
		AWeaponBase* myWeap = GetWorld()->SpawnActor<AWeaponBase>(SniperWeapon, FVector(0), FRotator(0));
		if (myWeap)
		{
			PlayerStateREF->SetWeapon2(myWeap);
		}
	}
}

void AMyCharacter::FireWeapon(bool Firing)
{
	if (PlayerStateREF->GetHoldGun() && !Sprint && !Dead && !Reload)
	{
		if (Firing && PlayerStateREF->GetHoldGun()->CurrentAmmo > 0)
		{
			IntervalMontageFire = PlayerStateREF->GetHoldGun()->FireInterval;
		
			PlayerStateREF->GetHoldGun()->FireWeapon();
			PlayMontageFiring();
			Fire = true;
			GetWorldTimerManager().SetTimer(MontageFireInterval, this, &AMyCharacter::PlayMontageFiring, IntervalMontageFire, true);
			GetWorldTimerManager().SetTimer(DispersionTimer, this, &AMyCharacter::Dispersion, IntervalMontageFire, true);
		}
		if (!Firing)
		{
			PlayerStateREF->GetHoldGun()->StopFire();
			Fire = false;
			if (Aiming)
			{
				UAnimMontage* StopMontage;
				StopMontage = FireIrosight;
				if (!Aiming)
				{
					StopMontage = FireHip;
					StopAnimMontage(StopMontage);
				}
			}
			IntervalMontageFire = 0.f;
			GetWorldTimerManager().ClearTimer(MontageFireInterval);
			GetWorldTimerManager().ClearTimer(DispersionTimer);
			
		}
	}
}

void AMyCharacter::ReloadingWeapon()
{
	if (PlayerStateREF->GetAmmo() > 0 && PlayerStateREF->GetHoldGun()->CurrentAmmo < PlayerStateREF->GetHoldGun()->MaxCurrentAmmo && !Dead && !Reload)
	{
		PlayerStateREF->GetHoldGun()->ReloadingWeapon();
		Reload = true;
		float TimeReload = 2.20f;
		GetWorldTimerManager().SetTimer(ReloadTimer, this, &AMyCharacter::StopReload, TimeReload, false);
		if (ReloadMontage)
		{
			PlayAnimMontage(ReloadMontage);
		}
		SetCurrentAmountAmmoWeapon();
	}
	else
	{
		UE_LOG(LogTemp,Warning,TEXT("NoAmmo"))
	}
}

void AMyCharacter::PlayMontageFiring()
{
	
		if (Aiming && FireIrosight)
		{
			PlayAnimMontage(FireIrosight);
			UE_LOG(LogTemp, Warning, TEXT("FireIrosight"));
		}
		else
		{
			if (!Aiming && FireHip)
			{
				PlayAnimMontage(FireHip);
				UE_LOG(LogTemp, Warning, TEXT("FireHip"));
			}
		}
}


	

void AMyCharacter::SetCurrentAmountAmmoWeapon()
{
	int32 AmmoChange = PlayerStateREF->GetHoldGun()->MaxCurrentAmmo - PlayerStateREF->GetHoldGun()->CurrentAmmo;
	if (PlayerStateREF->GetAmmo() >= AmmoChange)
	{
		PlayerStateREF->GetHoldGun()->CurrentAmmo += AmmoChange;
		PlayerStateREF->SetAmmo(-AmmoChange);
	}
	if (PlayerStateREF->GetAmmo() < AmmoChange)
	{
		AmmoChange = PlayerStateREF->GetAmmo();
		PlayerStateREF->GetHoldGun()->CurrentAmmo += AmmoChange;
		PlayerStateREF->SetAmmo(-AmmoChange);
	}
}

void AMyCharacter::WeaponChange1()
{
	if (PlayerStateREF->GetWeapon1())
	{
		FireWeapon(false);
		PlayerStateREF->GetHoldGun()->StopFire();
		float LengthMontage = EquipWeapon->GetPlayLength();
		PlayAnimMontage(EquipWeapon);
		GetWorldTimerManager().SetTimer(MontageEquipWeapon, this, &AMyCharacter::SetChangeWeapon1, LengthMontage, false);
	}
}

void AMyCharacter::WeaponChange2()
{
	if (PlayerStateREF->GetWeapon2())
	{
		FireWeapon(false);
		PlayerStateREF->GetHoldGun()->StopFire();
		float LengthMontage = EquipWeapon->GetPlayLength();
		PlayAnimMontage(EquipWeapon);
		GetWorldTimerManager().SetTimer(MontageEquipWeapon, this, &AMyCharacter::SetChangeWeapon2, LengthMontage, false);

	}
}

void AMyCharacter::SetChangeWeapon1()
{
	PlayerStateREF->SetWeapon2(PlayerStateREF->GetHoldGun());
	PlayerStateREF->SetHoldGun(PlayerStateREF->GetWeapon1());
	PlayerStateREF->SetWeapon1(nullptr);
}

void AMyCharacter::SetChangeWeapon2()
{
	PlayerStateREF->SetWeapon1(PlayerStateREF->GetHoldGun());
	PlayerStateREF->SetHoldGun(PlayerStateREF->GetWeapon2());
	PlayerStateREF->SetWeapon2(nullptr);
}

int32 AMyCharacter::MoneyAmount()
{
	return Money;
}

void AMyCharacter::SetMoneyAmount(int32 MoneyDicr)
{
	Money -= MoneyDicr;
}

void AMyCharacter::StopReload()
{
	Reload = false;
	PlayerStateREF->GetHoldGun()->StopReloadWeapon();
}

void AMyCharacter::Dispersion()
{
	if (!Aiming)
	{
		float Dis = FMath::RandRange(135.f, -135.f);
		PlayerStateREF->GetHoldGun()->Dispersion = FVector(0, Dis, 0);
	}
	if (Aiming)
	{
		PlayerStateREF->GetHoldGun()->Dispersion = FVector(0);
	}
}



float AMyCharacter::TakeDamage(float DamageAmount, FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser)
{
	float CurrentDamage = Super::TakeDamage(DamageAmount, DamageEvent, EventInstigator, DamageCauser);
	if (Health && !Dead)
	{
		Health->ChangeValueHealth(DamageAmount);
	}
	return CurrentDamage;
}



