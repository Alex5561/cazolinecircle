// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "MyPlayerController.generated.h"

class AMyCharacter;
class AMyPlayerState;


UCLASS()
class GASOLINECIRCLE_API AMyPlayerController : public APlayerController
{
	GENERATED_BODY()

private:
	AMyCharacter* MyCharacterREF = nullptr;
	AMyPlayerState* MyPlayerStateREF = nullptr;

	virtual void BeginPlay() override;

	

public:
	
	virtual void SetupInputComponent() override;
	virtual void Tick(float DeltaTime) override;

	
	

	void SetREFCharacter();//cast Char
	void SetREFPlayerState();//cast PlayerState
	UFUNCTION(BlueprintCallable,BlueprintPure,Category = "Character")
	AMyCharacter* GetCharacterREF();//Variable
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Character")
	AMyPlayerState* GetMyPlayerStateREF();//Variable

	void MovementCharacterForward(float input);//Input Char
	void MovementCharacterRight(float input);// Input Char
	void Aiming();//State Weapon
	void UnAiming();
	void Sprint();//Set
	void UnSprint();
	void FireInput();//Set Firing
	void UnFireInput();
	void Reload();//Set Reloading
	void SetWeapon1();//Change Weapon
	void SetWeapon2();//Change Weapon


	float AxisX;//Debug
	float AxisY;//Debug

	
};
